# PyMaze

## Maze building
This simple script produces a maze of desired dimensions. The three conditions that
all mazes must acomplish are:
* There is one and just one correct path that connects the entrance and the exit.
* There are no zeros, or circular plaths, in which one go arround an area once and again. In other words:
there are no disconected black pieces.
* Any point of the maze must be accessible, not allowing disconnected areas.

## Maze solving
The script also includes a function to solve a given maze. The followed methodology consists in the "right and left hand".
On a maze where the previous conditions are fulfilled, if you enter the maze and keep walking always with your right hand touching
the right wall, you will actually reach the way out no matter what. Then, if you do the same with your left hand, the path
you walked along the two iterations is the solution path.

This function also outputs two images with two colors representing the rigth and left hand paths and another for the
solution path.